import re
from .keys import keys, MODULES, MODULES_METHODS
from inspect import ismethod, ismemberdescriptor, ismethoddescriptor, isdatadescriptor, isgetsetdescriptor, isclass

TMP_FILENAME = 'tmp.py'
NEW_LINE_SEP_SPACED = ' !#NEW_LINE#! '
NEW_LINE_SEP = '!#NEW_LINE#!'


def replace_(string: str, replace_list: list):
    for elem in replace_list:
        repl_str = elem[0]
        new_str = elem[1]
        string = string.replace(repl_str, new_str)
    return string


def to_rus(keys: dict, source: str):
    print('Исходный текст:', source)
    for key, value in keys.items():
        source = re.sub('\s'+key+'\s', value, source)
    return source

def operators_to_eng(source: str):
    source = ' '+source
    source = source.replace('\r', NEW_LINE_SEP_SPACED)
    replacing = [['(', ' ( '], [')', ' ) '], [',', ' , '], ['.', ' . '], ]
    source = replace_(source, replacing)

    for key, value in keys.items():
        source = re.sub(value+r'\b', key+' ', source)

    for key, value in MODULES.items():
       source = re.sub(r'[\.\(\s\,]('+value+')[\.\)\s\,]', ' '+key+' ', source)

    source = source[1:]
    source = source.replace(NEW_LINE_SEP_SPACED, '\r')
    return source


def methods_to_eng(source: str, used_modules: list):
    for used_module in used_modules:
        used_module = used_module[0]
        for row in MODULES_METHODS.get(used_module):
            #print(row)
            method_ru = row[1]
            method_en = row[0]
            print(method_ru, method_en)
            source = re.sub(method_ru, ' '+method_en+' ', source)
    return source


def get_modules_list(source):
    source_strings = source.split('\n')
    imports = []
    for row in source_strings:
        if row.find(keys.get('import')) != -1 and row.find('из') != -1:
            row_splited = row.split(keys.get('import'))
            from_ = row_splited[0].replace(keys.get('from'), '').replace(' ', '')
            from_imports = row_splited[1].split(',')
            from_imports = list(map(lambda x: x.replace(' ', ''), from_imports))
            imports.append([from_, from_imports])

        if row.find('импортировать') != -1 and row.find('из') == -1:
            row = row.replace('импортировать', '').replace(' ', '')
            imports.append([row, []])
    return imports


def prepare_module(module):
    if not module[1]:
        string = 'import {}'.format(module[0])
    else:
        from_ = ','.join(module[1])
        string = 'from {} import {}'.format(module[0], from_)
    return string


def make_out_modules(modules):
    module_dirs = []
    modules_dict = {}
    for module in modules:
        exec('import '+module[0])
        module_dirs.append({module[0]: dir(eval(module[0]))})

    for row in module_dirs:
        for module, dirs in row.items():
            modules_dict[module] = []
            tmp_module = []
            for dir_ in dirs:
                tmp_dir = []
                for elem in dir(eval('{}.{}'.format(module, dir_))):
                    if dir_[0] == '_' or elem[0] == '_':
                        continue
                    module_dirs.append({})
                    tmp_dir.append(elem)
                modules_dict[module].append({dir_: tmp_dir})
    return modules_dict


def modules_name_to_eng(modules):
    modules_en = []
    for module in modules:
        from_ = module[0]
        imports_ = module[1]
        tmp = []

        for module_en, module_ru in MODULES.items():
            if module_ru == from_:
                tmp.append(module_en)
                break

        tmp.append([])
        for import_ in imports_:
            for module_en, module_ru in MODULES.items():
                if import_ == module_ru:
                    tmp[1].append(module_en)
        modules_en.append(tmp)
    return modules_en

def translate(source_raw, command_flag=True):
    if command_flag:
        if source_raw.find('myfile.write(') == -1: #
            return source_raw

        source = re.search("myfile.write\(b'(.*)\r", source_raw).group(1)
        command = source_raw.replace(source, '{}')
       # source = source.replace('\r', '\n') # ВОЗМОЖНА ОШИБКА, УБРАТЬ ЕСЛИ НЕ БУДЕТ РАБОТАТЬ
        command = command.replace('\r', '\n')
    else:
            source = source_raw
    modules = get_modules_list(source)
    modules = modules_name_to_eng(modules)
    eng = operators_to_eng(source)
    eng = methods_to_eng(eng, modules)
    if command_flag:
        source = command.format(eng)   # исходный код + команда
        return source
    else:
        return eng
